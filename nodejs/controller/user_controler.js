const User = require('../models/user_model')
const Room = require('../models/room_model')
const { validationResult } = require('express-validator')
const jwt = require('jsonwebtoken')
const response = require('../response/response')

const loginController = async (req, res) => {
    const errors = validationResult(req);
    const {username, profile_image, room_name} = req.body
    const user = await User.findOne({username: username})

    if (!errors.isEmpty()) {
        return res.json(errors);
    }

    const findRoom = await Room.findOne({room_name: room_name})
    var roomId;

    if (findRoom == null) {
        const createRoom = await Room.create({
            room_name: room_name,
        })
        roomId = createRoom._id
    } else {
        roomId = findRoom._id
    }
    
    if (user != null) {
        const updateUser = await User.findByIdAndUpdate({_id: user._id}, req.body)
        const token = jwt.sign({
            _id: updateUser._id,
        },
            process.env.SECRET_KEY,
            {
                expiresIn: '7 days'
            });

        const expires_in = jwt.decode(token);

        return res.json(response.successResponse({
            room_id: roomId,
            user_id: updateUser._id,
            access_token: token, //token
            expires_in: expires_in.exp, //วันหมดอายุ
            token_type: 'Bearer'
        }))
    } else {
        const createUser = await User.create(req.body);
        const token = jwt.sign({
            _id: createUser._id,
        },
            process.env.SECRET_KEY,
            {
                expiresIn: '7 days'
            });

        const expires_in = jwt.decode(token);

        return res.json(response.successResponse({
            room_id: roomId,
            user_id: createUser._id,
            access_token: token, //token
            expires_in: expires_in.exp, //วันหมดอายุ
            token_type: 'Bearer'
        }))
    }
}

const getProfileController = async (req, res) => {
    const data = {
        _id: req.user._id,
        username: req.user.username,
        profile_image: req.user.profile_image,
    }
    return res.json(response.successResponse(data))
}

module.exports = {
    loginController,
    getProfileController,
}