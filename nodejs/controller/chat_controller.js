const Room = require('../models/room_model')
const Chat = require('../models/chat_model')
const response = require('../response/response')
const { validationResult } = require('express-validator')

const getChatByRoomId = async (req, res) => {
    const {_id} = req.params
    const chat = await Room.findOne({_id: _id}).populate({
        path: 'chats',
        options: {
            sort: { _id: 'desc' }
        },
        populate: {
            path: 'users'
        }
    })
    return res.json(response.successResponse(chat))
}

const createChat = async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.json(errors);
    }

    req.body.user_id = req.user._id
    const chat = await Chat.create(req.body)
    return res.json(response.successResponse(chat))
}

module.exports = {
    getChatByRoomId,
    createChat,
}