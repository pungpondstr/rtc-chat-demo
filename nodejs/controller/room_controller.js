const Room = require('../models/room_model')
const response = require('../response/response')

const createRoom = async (req, res) => {
    const {room_name} = req.body

    const findRoom = await Room.findOne({room_name: room_name})

    if (findRoom != null) {
        
    } else {
        const room = await Room.create(req.body)
        return res.json(response.successResponse(room))
    }
}

module.exports = {
    createRoom,
}