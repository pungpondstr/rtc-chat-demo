require('dotenv').config()
const express = require('express')
const cors = require('cors')
const body = require('body-parser')
const mongoose = require('mongoose')
const socketio = require('socket.io')
const passport = require('passport')
const app = express()

mongoose.connect(`mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASS}@${process.env.MONGO_SERVER}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
});

app.use(cors())
app.use(body())
app.use(passport.initialize());

const index = require('./routes/index')
const user = require('./routes/user')
const chat = require('./routes/chat')

app.use('/', index)
app.use('/user', user)
app.use('/chat', chat)

const server = app.listen(process.env.SERVER_PORT, () => console.log(`server start port: ${process.env.SERVER_PORT}`))

const io = socketio(server, {
    cors: {
        origin: '*',
    }
})

io.sockets.on('connection', async function (socket) {
    console.log('user connected')

    socket.on('disconnect', () => {
        console.log('user disconnected');
    })

    socket.on('chat-all', async function (roomId) {
        const Room = require('./models/room_model')
        const chat = await Room.findOne({_id: roomId}).populate({
            path: 'chats',
            options: {
                sort: { _id: 'desc' }
            },
            populate: {
                path: 'users'
            }
        })
        socket.emit('chat-all', (chat))
        socket.broadcast.emit('chat-all', (chat))
    })
})