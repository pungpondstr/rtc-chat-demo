const successResponse = (data) => {
    return {
        "success": true,
		"msg":     "ok",
		"obj":     data,
    }
}

const errorResponse = (data) => {
    return {
        "success": false,
		"msg":     data == null ? "something went wrong" : data,
    }
}

module.exports = {
    successResponse,
    errorResponse,
}