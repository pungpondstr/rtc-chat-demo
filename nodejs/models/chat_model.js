const mongoose = require('mongoose');

const Chat = new mongoose.Schema({
    user_id: {
        type: String,
        default: null,
    },
    room_id: {
        type: String,
        default: null,
    },
    chat_type: {
        type: String,
        default: null, //chat, image
    },
    chat: {
        type: String,
        default: null,
    },
    image: {
        type: String, 
        default: null,
    },
}, 
{ 
    timestamps: true,
    collation: 'chats' 
}); 

Chat.virtual('users',{
    ref: 'User',
    localField: 'user_id',
    foreignField: '_id',
    justOne: true
});

Chat.set('toObject', { virtuals: true });
Chat.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Chat', Chat);