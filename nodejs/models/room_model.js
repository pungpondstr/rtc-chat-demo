const mongoose = require('mongoose');

const Room = new mongoose.Schema({
    room_name: {
        type: String,
        default: null,
    },
}, 
{ 
    timestamps: true,
    collation: 'rooms' 
}); 

Room.virtual('chats',{
    ref: 'Chat',
    localField: '_id',
    foreignField: 'room_id',
    // justOne: true
});

Room.set('toObject', { virtuals: true });
Room.set('toJSON', { virtuals: true });


module.exports = mongoose.model('Room', Room);