const mongoose = require('mongoose');

const User = new mongoose.Schema({
    username: {
        type: String,
        default: null,
    },
    profile_image: {
        type: String,
        default: null
    }
}, 
{ 
    timestamps: true,
    collation: 'users' 
}); 

module.exports = mongoose.model('User', User);