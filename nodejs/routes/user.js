const express = require('express')
const route = express.Router()
const userController = require('../controller/user_controler')
const authMiddleware = require('../middleware/auth_middleware')
const { check } = require('express-validator')

route.post('/login', [
    check('room_name').not().notEmpty().withMessage('Please input room'),
    check('username').not().notEmpty().withMessage('Please input username'),
    check('profile_image').not().notEmpty().withMessage('Please input profile image'),
], userController.loginController)

route.get('/profile', [authMiddleware.isLogin], userController.getProfileController)

module.exports = route