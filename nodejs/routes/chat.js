const express = require('express')
const route = express.Router()
const authMiddleware = require('../middleware/auth_middleware')
const chatController = require('../controller/chat_controller')
const { check } = require('express-validator')

route.get('/:_id', [authMiddleware.isLogin], chatController.getChatByRoomId)

route.post('/create', [
    check('room_id').not().notEmpty().withMessage('Please input room_id'),
    check('chat_type').not().notEmpty().withMessage('Please input chat_type'),
    check('chat_type').not().notEmpty().withMessage('Please input chat_type'),
    check('chat').not().notEmpty().withMessage('Please input chat'),
], [authMiddleware.isLogin], chatController.createChat)

module.exports = route