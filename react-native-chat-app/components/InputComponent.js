import { StyleSheet, TextInput, View } from "react-native";

export default InputComponent = (props) => {
    return (
        <View>
            <TextInput 
                placeholder={props.placeholder} 
                styles={styles.input}
                
            />
        </View>
    );
}

const styles = StyleSheet.create({
    input: {
      height: 40,
      margin: 12,
      borderWidth: 1,
      padding: 10
    },
});