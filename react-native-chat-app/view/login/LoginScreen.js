import { View, Text, Button, TextInput, StyleSheet, Image, Alert, TouchableNativeFeedback } from 'react-native';
import { API_URL } from "@env"
import axios from 'axios';
import React, { useState, useCallback, useEffect } from 'react';

const imageList = [
  'https://reactnative.dev/img/tiny_logo.png',
  'https://cdn-icons-png.flaticon.com/512/282/282100.png'
];

const LoginScreen = ({ navigation }) => {
  const [room, onchangeRoom] = useState('');
  const [username, onChangeUsername] = useState('');
  const [profileImage, onchangeProfileImage] = useState('');

  const login = async (username, profileImage) => {
    const response = await axios.post(`${API_URL}/user/login`, {
      'room_name': room,
      'username': username,
      'profile_image': profileImage
    })

    if (response.data.success) {
      navigation.navigate('Chat', {
        room_name: room,
        user: response.data,
      })
    } else {
      console.log(response.data)
      Alert.alert('Error', response.data.errors[0].msg);
    }
  }

  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text style={{ fontSize: 32 }}>Login Screen</Text>

      <View style={{ padding: 40 }}>
        <TextInput
          onChangeText={onchangeRoom}
          value={room}
          placeholder='Room'
          style={styles.input} />

          <View style={{ paddingBottom: 10 }}></View>

        <TextInput
          onChangeText={onChangeUsername}
          value={username}
          placeholder='Username'
          style={styles.input} />

        <View style={{ flexDirection: 'row', padding: 10, alignContent: 'center', justifyContent: 'center', alignItems: 'center' }}>
          {
            imageList.map((e) => <View style={{ padding: 5 }}>
              <TouchableNativeFeedback onPress={() => onchangeProfileImage(e)}>
                <Image
                  source={{
                    uri: e,
                    width: 50,
                    height: 50,
                  }}
                />
              </TouchableNativeFeedback>
            </View>)
          }

        </View>

      </View>

      <Button
        title="Go to Chat"
        onPress={() => login(username, profileImage)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    fontSize: 18,
    borderWidth: 1,
    borderColor:
      'grey',
    width: 250,
    padding: 15,
    borderRadius: 10,
    alignContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  }
})

export default LoginScreen;
