import React, { useState, useCallback, useEffect } from 'react';
import { GiftedChat, Bubble, InputToolbar, Actions, Send, Composer } from 'react-native-gifted-chat';
import { View, StyleSheet, Image, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { useNavigation } from '@react-navigation/native';
import { API_URL } from "@env"
import axios from 'axios';
import io from 'socket.io-client';

const ChatScreen = ({ route }) => {
  const { user, room_name } = route.params

  // console.log(user)
  const [messages, setMessages] = useState([]);
  const [status, setStatus] = useState(false);
  const navigation = useNavigation();

  const loadMessages = async () => {
    let config_token = {
      headers: {
        Authorization: "Bearer " + user['obj']['access_token'],
      },
    };

    const response = await axios.get(`${API_URL}/chat/${user['obj']['room_id']}`, config_token)
    if (response.data['success']) {
      let data = []
      for (let i = 0; i != response.data['obj']['chats'].length; i++) {
        data.push({
          _id: response.data['obj']['chats'][i]['_id'],
          text: response.data['obj']['chats'][i]['chat'],
          createdAt: response.data['obj']['chats'][i]['created_at'],
          user: {
            _id: response.data['obj']['chats'][i]['users']['_id'],
            name: response.data['obj']['chats'][i]['users']['username'],
            avatar: response.data['obj']['chats'][i]['users']['profile_image'],
          },
        })
      }
      setMessages(data);
    }
  };

  useEffect(() => {
    loadMessages();

    const socket = io(API_URL);

    socket.on('connect', () => {
      console.log('connected')
    })

    socket.on('chat-all', (data) => {
      loadMessages();
    })
  }, []);

  const onSend = useCallback(async (newMessages = []) => {
    console.log(newMessages)
    setMessages(previousMessages =>
      GiftedChat.append(previousMessages, newMessages)
    );

    // AsyncStorage.setItem(
    //   'messages',
    //   JSON.stringify(GiftedChat.append(messages, newMessages))
    // );
  }, [messages]);

  const sendMessage = async (message) => {
    setMessages(previousMessages =>
      GiftedChat.append(previousMessages, message)
    );

    let config_token = {
      headers: {
        Authorization: "Bearer " + user['obj']['access_token'],
      },
    };
    // console.log(message[0].text)
    const response = await axios.post(`${API_URL}/chat/create`, {
      "room_id": user['obj']['room_id'],
      "chat_type": "chat",
      "chat": message[0].text
    }, config_token).then(response => {
      if (response.data.success) {
        
        const socket = io(API_URL);

        socket.on('connect', () => {
          console.log('connected')
        })

        socket.emit('chat-all', user['obj']['room_id'])

        socket.on('disconnect', () => {
          console.log('dis-connected')
        })
      }
    })
  }


  const setPredefinedMessages = () => {
    const predefinedMessages = [
    ];
    setMessages(predefinedMessages);
  };

  const renderBubble = (props) => {
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          right: {
            backgroundColor: '#203454',
          },
          left: {
            backgroundColor: '#ffff',
          },
        }}
        textStyle={{
          right: {
            color: '#fff',
          },
          left: {
            color: '#3d3d3d',
          },
        }}
      />
    );
  };

  const renderInputToolbar = (props) => {
    return (
      <InputToolbar
        {...props}
        containerStyle={{
          borderRadius: 30,
          alignItems: 'flex-end',
          alignSelf: 'center',
          borderBlockColor: 'transparent'
        }}
        primaryStyle={{ alignItems: 'center' }}
      />
    );
  };

  const renderActions = (props) => {
    return (
      <Actions
        containerStyle={styles.actionButton}
        icon={() => (
          <View style={styles.bgIcon}>
            <Icon name="add" size={20} color="#ffff" />
          </View>
        )}
        onPress={() => {
          console.log('Image picker action');
        }}
      />
    );
  };

  const renderSend = (props) => {
    return (
      <Send {...props}>
        <View style={styles.sendButton}>
          <Icon name="send" size={28} color="#f47454" />
        </View>
      </Send>
    );
  };

  const renderComposer = (props) => {
    return (
      <View style={styles.composerContainer}>
        <Composer {...props} textInputStyle={styles.composer} />
      </View>
    );
  };

  const renderAvatar = ({ currentMessage = {}, position = 'left' }) => {
    if (position === 'left' && currentMessage.user?.avatar) {
      return (
        <Image
          // ถ้าเชื่อม API แล้วมาเปิดใช้ตัวด้านบน มันบัคไม่แสดงรูป
          // source={{ uri: currentMessage.user.avatar}}
          source={{ uri: 'https://cdn-icons-png.flaticon.com/512/3541/3541871.png' }}
          style={styles.avatar}
        />
      );
    }
    return null;
  };

  const Header = ({ icon, title, status }) => {
    return (
      <View style={styles.borderHeader}>
        <View style={styles.borderTitle}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Icon name="arrow-back" size={25} />
          </TouchableOpacity>

          <Image
            source={{ uri: icon }}
            style={styles.headerAvatar}
          />
          <View style={styles.subborderTitle}>
            <Text style={styles.titleName}>{title}</Text>
            <View style={styles.bordersub}>
              <Icon name="circle" size={12} color={status ? "#f60707" : "#2d6951"} />
              <Text style={styles.subTitle}>Online</Text>
            </View>
          </View>
        </View>
        <View style={styles.headerIcons}>
          <TouchableOpacity onPress={setPredefinedMessages}>
            <Icon name="more-vert" size={25} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.circle} />
      <Header
        icon="https://reactnative.dev/img/tiny_logo.png"
        title={'Room: ' + room_name}
        status={status}
      />
      <View style={styles.body}>
        <View style={styles.inside}>
          <GiftedChat
            messages={messages}
            onSend={messages => sendMessage(messages)}
            user={{
              _id: user['obj']['user_id'],
            }}
            renderBubble={renderBubble}
            renderInputToolbar={renderInputToolbar}
            renderActions={renderActions}
            renderSend={renderSend}
            renderAvatar={renderAvatar}
            renderComposer={renderComposer}
            showAvatarForEveryMessage={true}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f7edda',
    position: 'relative'
  },
  circle: {
    position: 'absolute',
    backgroundColor: 'red',
    width: '70%',
    height: '50%',
    borderTopRightRadius: 500,
    borderBottomRightRadius: 500,
    top: -150,
    backgroundColor: '#ffcc54',

  },
  body: {
    flex: 1,
    backgroundColor: '#fff4e4',
    borderTopRightRadius: 40,
    borderTopLeftRadius: 40,
  },
  inside: {
    flex: 1,
    paddingBottom: 10,
    marginHorizontal: 10,
    marginTop: 20,
    marginBottom: 5
  },
  avatar: {
    width: 40,
    height: 40,
    borderRadius: 20,
    margin: 3,
  },
  headerAvatar: {
    width: 40,
    height: 40,
    borderRadius: 20,
    margin: 10,
    alignSelf: 'flex-start',
  },
  actionButton: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 15,
    marginTop: 6,
  },
  sendButton: {
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 15,
    marginBottom: 10,
  },
  composerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: 25,
    flex: 1,
  },
  composer: {
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
    paddingTop: 7,
  },
  emojiButton: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  borderHeader: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  titleName: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  subTitle: {
    fontSize: 14,
    marginLeft: 5,
  },
  bordersub: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  borderTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  subborderTitle: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  bgIcon: {
    backgroundColor: '#ff6b5d',
    borderRadius: 5,
    padding: 3,
  },
  headerIcons: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default ChatScreen;
