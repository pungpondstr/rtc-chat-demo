import React from 'react';
import { SafeAreaView, StyleSheet, useColorScheme } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

// screen components
import LoginScreen from './view/login/LoginScreen';
import ChatScreen from './view/chat/ChatScreen';

const Stack = createNativeStackNavigator();

const screens = [
  {
    name: "Login",
    component: LoginScreen,
    title: "Login",
  },
  {
    name: "Chat",
    component: ChatScreen,
    title: "Chat",
  },
];

export default function App() {
  const scheme = useColorScheme(); // Get the current system theme

  return (
    <SafeAreaView style={[styles.safeArea, scheme === 'dark' ? styles.darkBackground : styles.lightBackground]}>
      <NavigationContainer>
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          {screens.map((screen, index) => (
            <Stack.Screen
              key={index}
              name={screen.name}
              component={screen.component}
              options={{ title: screen.title }}
            />
          ))}
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  safeArea: {
    height: '103%'
  },
  lightBackground: {
    backgroundColor: '#ffffff', 
    color: '#000'
  },
  darkBackground: {
    backgroundColor: '#000000', 
    color: '#ffff'
  },
});
